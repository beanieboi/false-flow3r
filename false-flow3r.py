import math

import leds
import st3m.run
from st3m.reactor import Responder
from st3m.utils import tau

NUM_LEDS = 40
MAX_VELOCITY = 50.0  # px/s
WORLD_TILE_SIZE = 40.0
WORLD_TILE_COLORS = [(0, 0.7, 0), (0, 0.4, 0)]
BALL_SIZE = 20.0
BALL_COLOR = (1.0, 1.0, 1.0)
HOLE_SIZE = 30.0
HOLE_COLOR = (0.1, 0.1, 0.1)
DISPLAY_WIDTH = 240.0
DISPLAY_HEIGHT = 240.0


class State:
    PLAYING = 1
    GAME_OVER = 2


class FalseFlag(Responder):
    def __init__(self) -> None:
        self.reset()
        self.state = State.PLAYING

    def draw(self, ctx: Context) -> None:
        if self.state == State.PLAYING:
            self.world.draw(ctx, self.viewport)
            self.ball.draw(ctx)
        else:
            ctx.rgb(0, 0, 0).rectangle(
                self.viewport.min_x,
                self.viewport.min_y,
                self.viewport.width,
                self.viewport.height,
            ).fill()

    def reset(self):
        self.viewport = Rect(
            Vec2(
                -DISPLAY_WIDTH / 2.0,
                -DISPLAY_HEIGHT / 2.0,
            ),
            Vec2(
                DISPLAY_WIDTH,
                DISPLAY_HEIGHT,
            ),
        )
        self.world = World()
        self.ball = Ball()

    def think(self, ins: InputState, delta_ms: int) -> None:
        if self.state == State.PLAYING:
            self.ball.think(ins, delta_ms)
            self.viewport.center = self.ball.pos

            for hole in self.world.iter_holes_in_viewport(self.viewport):
                if self.ball.in_hole(hole):
                    self.game_over()
                    break

    def game_over(self):
        self.reset()
        self.state = State.GAME_OVER


class Ball(Responder):
    def __init__(self) -> None:
        self.pos = Vec2(0.0, 0.0)
        self.v = Vec2(0.0, 0.0)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(*BALL_COLOR).arc(
            0,
            0,
            BALL_SIZE / 2.0,
            0,
            tau,
            0,
        ).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        f = delta_ms / 1000.0
        self.v.x += ins.imu.acc[1] * 20.0 * f
        self.v.y += ins.imu.acc[0] * 20.0 * f
        self.pos += self.v * f

    def in_hole(self, hole_center: Vec2):
        distance = (self.pos - hole_center).length()
        return distance < HOLE_SIZE / 2.0


class World:
    def draw(self, ctx: Context, viewport: Rect):
        self._draw_grid(ctx, viewport)
        self._draw_holes(ctx, viewport)

    def _draw_grid(self, ctx: Context, viewport: Rect):
        min_x = math.floor(viewport.min_x / WORLD_TILE_SIZE) - 1
        min_y = math.floor(viewport.min_y / WORLD_TILE_SIZE) - 1
        max_x = math.ceil(viewport.max_x / WORLD_TILE_SIZE) + 1
        max_y = math.ceil(viewport.max_y / WORLD_TILE_SIZE) + 1
        for x in range(min_x, max_x + 1):
            for y in range(min_y, max_y + 1):
                color = WORLD_TILE_COLORS[(x + y) % 2]
                ctx.rgb(*color).rectangle(
                    x * WORLD_TILE_SIZE - viewport.center.x,
                    y * WORLD_TILE_SIZE - viewport.center.y,
                    WORLD_TILE_SIZE,
                    WORLD_TILE_SIZE,
                ).fill()

    def _draw_holes(self, ctx: Context, viewport: Rect):
        for hole in self.iter_holes_in_viewport(viewport):
            ctx.rgb(*HOLE_COLOR).arc(
                hole.x - viewport.center.x,
                hole.y - viewport.center.y,
                HOLE_SIZE / 2.0,
                0,
                tau,
                0,
            ).fill()

    def iter_holes_in_viewport(self, viewport: Rect):
        grid_size = WORLD_TILE_SIZE
        min_x = math.floor(viewport.min_x / grid_size) - 1
        min_y = math.floor(viewport.min_y / grid_size) - 1
        max_x = math.ceil(viewport.max_x / grid_size) + 1
        max_y = math.ceil(viewport.max_y / grid_size) + 1
        for x in range(min_x, max_x + 1):
            for y in range(min_y, max_y + 1):
                if ((x % 17 == 1 and y % 13 == 1) or
                    (x % 7 == 3 and y % 5 == 3)):
                    yield Vec2(x * grid_size, y * grid_size)


class Vec2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __add__(self, other):
        return Vec2(self.x + other.x, self.y + other.y)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def __sub__(self, other):
        return Vec2(self.x - other.x, self.y - other.y)

    def __mul__(self, a):
        return Vec2(self.x * a, self.y * a)

    def __truediv__(self, a):
        return Vec2(self.x / a, self.y / a)


class Rect:
    def __init__(self, pos: Vec2, size: Vec2):
        self.pos = pos
        self.size = size

    @property
    def min_x(self):
        return self.pos.x

    @property
    def max_x(self):
        return self.pos.x + self.size.x

    @property
    def min_y(self):
        return self.pos.y

    @property
    def max_y(self):
        return self.pos.y + self.size.y

    @property
    def center(self):
        return self.pos + self.size / 2.0

    @center.setter
    def center(self, center: Vec2):
        self.pos = center - self.size / 2.0

    @property
    def width(self):
        return self.size.x

    @property
    def height(self):
        return self.size.y


st3m.run.run_responder(FalseFlag())
